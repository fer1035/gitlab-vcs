provider "aws" {
  alias      = "dev"
  region     = "us-east-1"
  access_key = data.hcp_vault_secrets_app.warpedlenses.secrets.dev_access_key
  secret_key = data.hcp_vault_secrets_app.warpedlenses.secrets.dev_secret_key

  default_tags {
    tags = {
      env  = "sandbox"
      tier = "dev"
    }
  }
}

# module "dev_credentials" {
#   source = "app.terraform.io/fer1035/dynamic-credentials/aws"

#   providers = {
#     aws = aws.dev
#   }

#   tfc_organization_name = "fer1035"
#   tfc_project_name      = "dev"
#   tfc_run_phase         = "*"

#   aws_policy_actions = ["*"]
# }

# module "dev_workspace" {
#   source = "app.terraform.io/fer1035/project-workspace-governor/tfe"

#   org_name     = "fer1035"
#   project_name = "dev"

#   variable_sets = [
#     module.dev_credentials.variable_set_name
#   ]

#   workspace_configurations = {
#     dev_sandbox = {
#       trigger_source = null
#       tags           = ["dev", "sandbox"]
#     }
#   }

#   depends_on = [
#     module.dev_credentials
#   ]
# }

# output "dev_workspace" {
#   value = module.dev_workspace.environment
# }
