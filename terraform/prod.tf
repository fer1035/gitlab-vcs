provider "aws" {
  alias      = "prod"
  region     = "us-east-1"
  access_key = data.hcp_vault_secrets_app.warpedlenses.secrets.prod_access_key
  secret_key = data.hcp_vault_secrets_app.warpedlenses.secrets.prod_secret_key

  default_tags {
    tags = {
      env  = "sandbox"
      tier = "prod"
    }
  }
}

# module "prod_credentials" {
#   source = "app.terraform.io/fer1035/dynamic-credentials/aws"

#   providers = {
#     aws = aws.prod
#   }

#   tfc_organization_name = "fer1035"
#   tfc_project_name      = "prod"
#   tfc_run_phase         = "*"

#   aws_policy_actions = [
#     "sts:GetCallerIdentity"
#   ]
# }

# module "prod_workspace" {
#   source = "app.terraform.io/fer1035/project-workspace-governor/tfe"

#   org_name     = "fer1035"
#   project_name = "prod"

#   variable_sets = [
#     module.prod_credentials.variable_set_name
#   ]

#   workspace_configurations = {
#     prod_sandbox = {
#       trigger_source = null
#       tags           = ["prod", "sandbox"]
#     }
#   }

#   depends_on = [
#     module.prod_credentials
#   ]
# }

# output "prod_workspace" {
#   value = module.prod_workspace.environment
# }
