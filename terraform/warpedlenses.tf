provider "aws" {
  alias      = "warpedlenses"
  region     = "us-east-1"
  access_key = data.hcp_vault_secrets_app.warpedlenses.secrets.wl_access_key
  secret_key = data.hcp_vault_secrets_app.warpedlenses.secrets.wl_secret_key

  default_tags {
    tags = {
      env  = "warpedlenses"
      tier = "warpedlenses"
    }
  }
}

module "wl_credentials" {
  source = "app.terraform.io/fer1035/dynamic-credentials/aws"

  providers = {
    aws = aws.warpedlenses
  }

  tfc_organization_name = "fer1035"
  tfc_project_name      = "warpedlenses"
  tfc_run_phase         = "*"

  aws_policy_actions = [
    "sts:GetCallerIdentity"
  ]
}

module "wl_workspace" {
  source = "app.terraform.io/fer1035/project-workspace-governor/tfe"

  org_name     = "fer1035"
  project_name = "warpedlenses"

  variable_sets = [
    module.wl_credentials.variable_set_name
  ]

  workspace_configurations = {
    warpedlenses_primary = {
      trigger_source = null
      tags           = ["warpedlenses", "primary"]
    }
  }

  depends_on = [
    module.wl_credentials
  ]
}

output "wl_workspace" {
  value = module.wl_workspace.environment
}
