data "hcp_vault_secrets_app" "warpedlenses" {
  app_name = "WarpedLenses"
}
provider "tfe" {
  token = data.hcp_vault_secrets_app.warpedlenses.secrets.tfe_token
}
